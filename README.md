# Yrkeslegitimation #
Tjänstekontrakt för HoSp-registret. Yrkeslegitimation, förskrivningsrätt, specialistutbildning och behörighetsbegränsningar. 

### Status på detta repository ###

* Version: 1.1 


### Kontaktpersoner ###

* David Svärd, Socialstyrelsen
* Birgitta Ollars, Socialstyrelsen
* Oskar Thunman, Callista Enterprise